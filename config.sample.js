exports.host = '192.168.1.2';
exports.port = 8443;
exports.ircPort = 6697;
exports.ircHost = 'irc.example.net';
exports.password = "foobar";
exports.redirectUrl = "http://webirc.oftc.net";
exports.module = 'ws';
exports.reconnectTime = 15 * 1000;
